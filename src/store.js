import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    days: 28,
    blanks: 5,
    events: []
  },
  mutations: {
    addEvent (state, event) {
      state.events.push(event)
    }
  },
  actions: {
    addEvent ({ commit, state }, event) {
      commit('addEvent', event)
    }
  }
})
